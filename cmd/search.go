/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"fmt"
	"log"

	"goattr/pkg"

	"github.com/spf13/cobra"
)

// searchCmd represents the search command
var searchCmd = &cobra.Command{
	Use:   "search",
	Short: "Search for tag(s)",
	Long: `Resursivly search for tag(s)`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("search called")
        files, err := pkg.SearchTags(".", attribute, tags )
        if err != nil{
            log.Fatal(err)
        }
        fmt.Println("Contains tag:")
        for _, file := range files{
            fmt.Println(file)
        }
	},
}

func init() {
	rootCmd.AddCommand(searchCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// searchCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// searchCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
