package cmd

import (
	"fmt"
	"os"
	"github.com/spf13/cobra"
)

var cfgFile string
var filename string
var tags []string
const attribute string = "user.tags"

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "goattr",
	Short: "This CLI tool manages extended file attributes, allowing users to add, list, update, and remove serialized JSON tags from files.",
	Long: `
    This tool provides a comprehensive suite of functionalities for managing 
    extended attributes on files within filesystems that support such features. 
    Specifically, the tool focuses on manipulating serialized JSON data, 
    allowing users to dynamically add, list, update, and delete JSON-formatted 
    tags associated with files. These operations facilitate the organization 
    and retrieval of metadata directly linked to the file system, enhancing 
    file management and data handling capabilities for developers and system 
    administrators. The tool's robust functions include viewing current 
    attributes, adding new tags as JSON structures, modifying existing tags by 
    removing specified elements, and completely removing attributes when they 
    are no longer needed. This makes it particularly useful in environments 
    where file metadata plays a critical role in operations and data 
    governance.`,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	// Run: func(cmd *cobra.Command, args []string) { },
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize()

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	// rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
    rootCmd.PersistentFlags().StringVarP(&filename,"file", "f", "", "File")
    rootCmd.PersistentFlags().StringSliceVarP(&tags, "tags", "t", tags, "Tags")

}


