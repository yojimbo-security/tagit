/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"fmt"
	"log"

	"goattr/pkg"

	"github.com/spf13/cobra"
)

var maxDepth int

// listCmd represents the list command
var listCmd = &cobra.Command{
	Use:   "list",
	Short: "List all tag(s)",
    Long: `List all tag(s) recursively`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("list called")
        tags, err := pkg.ListTags(".", attribute, maxDepth)
        if err != nil{
            log.Panic(err)
        }
        for _, tag := range tags {
            fmt.Println(tag)
        }
	},
}

func init() {
	rootCmd.AddCommand(listCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// listCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// listCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
    listCmd.Flags().IntVarP(&maxDepth, "maxdepth", "m", -1, "Max depth to search directories")
}
