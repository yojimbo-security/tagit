/*
Package attr manages extended attributes specifically for storing and manipulating custom
tag data associated with files. This package provides a simplified interface for adding,
removing, and querying tags stored as JSON-encoded data in file system extended attributes.

Extended attributes in this context are used to store additional metadata about files, similar
to how one might store metadata in a database, but directly within the file system. This allows
applications to store and retrieve metadata efficiently without requiring a separate database.

The package provides straightforward functions such as AddTags, RemoveTag, GetTags, and SearchTags
that abstract the underlying complexity of extended attribute manipulation. These functions handle
the serialization and deserialization of JSON data to and from the extended attributes, ensuring data
integrity and simplifying access to the tag data.

Example usage includes adding tags to files for categorization, searching files based on these tags,
and modifying or removing tags from files. This functionality is crucial for applications that need
to manage additional metadata for files in a persistent, non-invasive manner.

Functions in this package:
- AddTags: Adds new tags to a file's attribute, avoiding duplicates.
- RemoveTag: Removes specified tags from a file's attribute.
- GetTags: Retrieves all tags from a file's attribute.
- SearchTags: Searches through directories to find files with specified tags.

This package assumes that the file system supports extended attributes and that the user has
the necessary permissions to modify these attributes.

Note: Error handling in this package is aggressive, with most errors resulting in logging and
termination of the application. This approach is chosen to ensure that attribute manipulation errors
are handled promptly, reflecting the importance of data integrity in applications using this package.
*/

package pkg

import (
	"encoding/json"
	"goattr/log"
	"os"
	"path/filepath"
	"strings"

	"github.com/pkg/xattr"
)

// Attrs defines a structure to hold tags as a slice of strings.
// The structure is designed to be serialized into and from JSON format.
type Attrs struct {
	Tags []string `json:"tags"` // Tags is a slice of strings that stores individual tags.
}

// AddTags appends new, non-duplicate tags to a list stored in a specified extended attribute of a file.
//
// This function reads the specified extended attribute from a file, unmarshals it into an Attrs struct,
// appends any new tags that are not already present, and then writes the updated list back to the file's
// extended attribute. If the attribute does not exist, it is created with an empty tag list before adding
// new tags.
//
// Params:
// - filename: The path to the file whose attribute is to be modified.
// - attribute: The name of the extended attribute where tags are stored.
// - newTags: A slice of strings containing the new tags to be added to the attribute.
//
// The function performs several operations:
// 1. Attempts to retrieve the current attribute value; initializes it if not found.
// 2. Unmarshals the JSON data into an Attrs struct.
// 3. Appends new, non-duplicate tags to the Attrs struct.
// 4. Marshals the updated Attrs struct back into JSON.
// 5. Sets the updated JSON as the new value of the extended attribute.
// 6. Retrieves the attribute after updating to log the current state for verification.
//
// Errors at any step (retrieval, unmarshalling, marshalling, or updating) are considered fatal,
// and the function will log a fatal error and terminate the application. This ensures that failures
// in attribute modification are not silently ignored and that the integrity of the operation is maintained.
//
// Example usage:
// err := AddTags("/path/to/file.txt", "user.tags", []string{"new_project", "important"})
// if err != nil {
//     log.Fatalf("Failed to add tags: %s", err)
// }
//
// Note: The aggressive use of log.Fatalf implies that any error in this function is considered
// unrecoverable within the context of the calling application. In a production environment,
// consider returning errors to allow calling code to handle them more flexibly.
func AddTags(filename string, attribute string, newTags []string) {
    currentData, err := xattr.Get(filename, attribute)
	if err != nil {
		// Assume error is because attribute does not exist yet
		currentData = []byte(`{"tags":[]}`)
	}

	var attrs Attrs
	if err := json.Unmarshal(currentData, &attrs); err != nil {
		log.Fatalf("Error unmarshaling JSON: %s", err)
	}

    // Loop through newTags and append each tag that is not already present.
	for _, newTag := range newTags {
		if !contains(attrs.Tags, newTag) {
			attrs.Tags = append(attrs.Tags, newTag)
		}
	}

	updatedData, err := json.Marshal(attrs)
	if err != nil {
		log.Fatalf("Error marshaling JSON: %s", err)
	}

	if err := xattr.Set(filename, attribute, updatedData); err != nil {
		log.Fatalf("Error setting attribute: %s", err)
	}

	data, err := xattr.Get(filename, attribute)
	if err != nil {
		log.Fatalf("Error getting attribute: %s", err)
	}
	log.Printf("Attribute %s set with value %s\n", attribute, string(data))
}

// contains determines whether a specific item is present within a slice of strings.
//
// This function iterates through each element in the provided slice and compares it to the
// item. If a match is found, the function returns true, indicating the item is present in the slice.
// If the end of the slice is reached without finding a match, the function returns false.
//
// Params:
// - slice: A slice of strings to be searched.
// - item: The string to search for within the slice.
//
// Returns:
// - A boolean value indicating whether the item is found in the slice. It returns true if the item
//   is present, and false otherwise.
//
// This function performs a linear search on the slice, which operates in O(n) time complexity where
// n is the length of the slice. This is efficient for small to medium-sized slices but may become
// inefficient for very large slices or when searches are performed repeatedly on the same data set.
//
// Example usage:
// fruits := []string{"apple", "banana", "cherry"}
// hasApple := contains(fruits, "apple")
// if hasApple {
//     fmt.Println("Apple is in the list of fruits.")
// } else {
//     fmt.Println("Apple is not in the list of fruits.")
// }
func contains(slice []string, item string) bool {
	for _, a := range slice {
		if a == item {
			return true
		}
	}
	return false
}

// GetTags retrieves the JSON-encoded data stored in an extended attribute of a file
// and unmarshals it into an Attrs struct.
//
// This function attempts to read the specified extended attribute from the provided file. If successful,
// it then tries to deserialize the JSON-encoded attribute value into an Attrs struct, which is a user-defined
// type designed to hold structured data extracted from file attributes.
//
// Params:
// - filename: The path to the file from which the attribute should be retrieved.
// - attribute: The name of the extended attribute that contains the serialized JSON data.
//
// Returns:
// - An Attrs struct populated with the data retrieved from the attribute.
// - An error if there is any issue in reading the attribute or unmarshaling the JSON data.
//
// This function will log a fatal error and terminate the program if JSON unmarshaling fails,
// indicating potentially corrupt or improperly formatted JSON data. This aggressive error handling
// is critical in scenarios where data integrity is paramount and errors in attribute data cannot
// be silently ignored or handled without immediate attention.
//
// Example usage:
// attrs, err := GetTags("/path/to/file.txt", "user.details")
// if err != nil {
//     log.Printf("Failed to retrieve attribute: %s", err)
//     return
// }
// fmt.Println("Retrieved attributes:", attrs)
func GetTags(filename, attribute string) (Attrs, error) {
	var retrievedData Attrs
	value, err := xattr.Get(filename, attribute)
	if err := json.Unmarshal(value, &retrievedData); err != nil {
		log.Fatal(err)
	}
	if err != nil {
		return Attrs{}, err
	}
	return retrievedData, nil
}

// RemoveTags modifies an extended attribute by removing specific values from it.
//
// This function accesses a specified attribute of a file, removes certain values (tags) from it,
// and updates the attribute with the new set of values. It handles the entire process of fetching
// the current attribute values, filtering out the specified values, serializing the remaining values
// back into JSON, and setting the updated JSON as the attribute's new value.
//
// Params:
// - filename: The path to the file from which the attribute values are to be modified.
// - attribute: The name of the extended attribute that contains the values (tags).
// - valuesToRemove: A slice of strings representing the values (tags) to remove from the attribute.
//
// The function will log a fatal error and terminate the program if it encounters issues at any step,
// including retrieving the current attribute value, marshaling data into JSON, or setting the updated attribute.
// This behavior ensures that failures in attribute modification are clearly indicated and not silently ignored.
//
// Example usage:
// RemoveTags("/path/to/file.txt", "user.tags", []string{"deprecated", "old_tag"})
// This call will remove the 'deprecated' and 'old_tag' values from the 'user.tags' attribute of 'file.txt'.
//
// Note: As with DeleteAttribute, the use of log.Fatalf for error handling is aggressive;
// it will cause the program to exit if the attribute modification fails. Consider handling errors
// more gracefully in a production environment to allow for recovery or alternative actions.
func RemoveTags(filename string, attribute string, valuesToRemove []string) {
	// Retrieve the current attribute value
	currentAttrs, err := GetTags(filename, attribute)
	if err != nil {
		log.Fatalf("Error retrieving attribute: %s", err)
		return
	}

	// Filter out the values that need to be removed
	newTags := []string{}
	for _, tag := range currentAttrs.Tags {
		remove := false
		for _, val := range valuesToRemove {
			if tag == val {
				remove = true
				break
			}
		}
		if !remove {
			newTags = append(newTags, tag)
		}
	}

	// Update the struct with the new list of tags
	currentAttrs.Tags = newTags

	// Serialize the updated data to JSON
	updatedValue, err := json.Marshal(currentAttrs)
	if err != nil {
		log.Fatalf("Error marshaling JSON: %s", err)
		return
	}

	// Set the updated attribute value
	if err := xattr.Set(filename, attribute, updatedValue); err != nil {
		log.Fatalf("Error setting updated attribute: %s", err)
	}
}

// DeleteAttribute removes a specified extended attribute from a file.
//
// This function attempts to delete an extended attribute identified by 'attribute' from the file specified by 'filename'.
// It is designed to handle file systems that support extended attributes and will fail if the attribute does not exist
// or if there are permissions issues preventing the attribute's removal.
//
// Params:
// - filename: The path to the file from which the attribute is to be removed.
// - attribute: The name of the extended attribute to remove.
//
// This function does not return any value. If an error occurs during the removal of the attribute,
// the function logs a fatal error and terminates the application. This behavior is suitable for
// applications where a failure to remove an attribute is considered critical and not recoverable within the same execution context.
//
// Example usage:
// DeleteAttribute("/path/to/file.txt", "user.description")
// This call will remove the 'user.description' attribute from 'file.txt' if it exists.
//
// Note: The use of log.Fatalf for error handling is aggressive; it will cause the program to exit if the attribute removal fails.
// In a production environment, it may be more appropriate to return the error to the caller for more nuanced handling.
func DeleteAttribute(filename string, attribute string) {
	if err := xattr.Remove(filename, attribute); err != nil {
		log.Fatalf("Error removing attribute: %s", err)
	}
}

// SearchTags searches through the filesystem starting from rootDir and identifies files
// that contain any of the specified tags within a specific extended attribute.
//
// This function performs a recursive walk through all directories starting from rootDir,
// checking each file to see if it contains any of the specified tags in the given extended attribute.
// If a file contains one or more of the search tags, its path is added to the result list.
//
// Params:
// - rootDir: The root directory from which to start the search.
// - attribute: The name of the extended attribute to inspect for tags.
// - searchTags: A slice of strings representing the tags to search for in the file's attributes.
//
// Returns:
// - A slice of strings containing the paths of all files that have any of the specified tags.
// - An error if there is a failure during the directory walk, such as an error accessing a directory.
//
// The function continues to traverse through the directory even if an error occurs while checking
// tags for a particular file, ensuring that one file's error does not halt the entire search process.
// Errors encountered during the walk that prevent access to a directory or file will halt the process
// and return an error.
//
// Example usage:
// files, err := SearchTags("/path/to/start", "user.tags", []string{"project", "documentation"})
// if err != nil {
//     log.Fatalf("Failed to search files: %s", err)
// }
// for _, file := range files {
//     fmt.Println("File with tags:", file)
// }
func SearchTags(rootDir, attribute string, searchTags []string) ([]string, error) {
	var filesWithTags []string

	// WalkFunc to process each file
	err := filepath.Walk(rootDir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err // Handle the error and continue with the walk
		}
		if !info.IsDir() {
			hasTags, err := fileHasTags(path, attribute, searchTags)
			if err != nil {
				return nil // Continue walking even if there's an error reading tags
			}
			if hasTags {
				filesWithTags = append(filesWithTags, path)
			}
		}
		return nil
	})

	if err != nil {
		return nil, err
	}

	return filesWithTags, nil
}

// fileHasTags determines whether any of the specified tags exist in the extended attributes of a file.
//
// This function reads the JSON-encoded tags from the specified extended attribute of a file and checks
// if any of the tags in the searchTags slice are present. It utilizes a set for efficient lookup.
//
// Params:
// - filename: The path to the file whose tags are to be checked.
// - attribute: The name of the extended attribute where the tags are stored as a JSON-encoded string.
// - searchTags: A slice of strings representing the tags to search for in the file's attributes.
//
// Returns:
// - A boolean value indicating whether any of the search tags are present in the file's tags.
// - An error if there is a failure in retrieving or processing the tags, such as a problem with
//   reading the extended attribute or unmarshaling the JSON data.
//
// The function first attempts to retrieve and unmarshal the attribute data. If successful, it checks
// for the presence of any specified tag in the attribute. It returns true as soon as one is found,
// otherwise, it returns false after checking all tags. Errors in accessing the attribute or during
// JSON unmarshalling result in returning an error.
//
// Example usage:
// hasTags, err := fileHasTags("example.txt", "user.tags", []string{"project", "documentation"})
// if err != nil {
//     log.Fatalf("Error checking tags: %s", err)
// }
// if hasTags {
//     fmt.Println("The file contains one or more of the search tags.")
// } else {
//     fmt.Println("None of the search tags were found in the file.")
// }
func fileHasTags(filename, attribute string, searchTags []string) (bool, error) {
	value, err := xattr.Get(filename, attribute)
	if err != nil {
		return false, err // File may not have any attributes or an error occurred
	}

	var attrs Attrs
	if err := json.Unmarshal(value, &attrs); err != nil {
		return false, err // Error unmarshaling JSON
	}

	tagSet := make(map[string]struct{})
	for _, tag := range attrs.Tags {
		tagSet[tag] = struct{}{}
	}

	for _, tag := range searchTags {
		if _, found := tagSet[tag]; found {
			return true, nil
		}
	}

	return false, nil
}

// fetchTags retrieves the tags stored in the specified extended attribute of a file.
//
// This function reads the extended attribute from the specified file and unmarshals the JSON-encoded
// data into an Attrs struct. The function is designed to fetch and return the tags as a slice of strings.
//
// Params:
// - filename: The path to the file from which tags need to be fetched.
// - attribute: The name of the extended attribute that contains the tags.
//
// Returns:
// - A slice of strings containing the tags stored in the extended attribute.
// - An error if the attribute cannot be read or if the JSON data cannot be successfully unmarshaled.
//
// The function returns an error if the specified attribute does not exist on the file, or if
// there are issues accessing the file's attributes. It also handles and reports errors related to
// JSON unmarshalling of the attribute's content.
//
// Example usage:
// tags, err := fetchTags("/path/to/file.txt", "user.tags")
// if err != nil {
//     log.Printf("Error fetching tags: %s", err)
//     return
// }
// fmt.Println("Tags:", tags)
func fetchTags(filename, attribute string) ([]string, error) {
	data, err := xattr.Get(filename, attribute)
	if err != nil {
		return nil, err // Handle errors related to fetching the extended attribute
	}
	var attrs Attrs
	if err := json.Unmarshal(data, &attrs); err != nil {
		return nil, err // Handle errors related to JSON unmarshalling
	}
	return attrs.Tags, nil // Return the list of tags extracted from the attribute
}

// ListTags searches through the filesystem starting from rootDir and lists all unique tags 
// stored in the specified extended attribute. It restricts the search depth to maxDepth.
//
// This function performs a filesystem walk starting at rootDir and examines each file's
// specified extended attribute for tags. It collects all unique tags into a slice. The search
// can be limited in depth, where a maxDepth of -1 indicates no limit. Directories deeper than
// maxDepth are not traversed. The function handles permission errors by logging them and
// skipping the inaccessible directories without halting the entire process.
//
// Params:
// - rootDir: The root directory from which the search begins.
// - attribute: The extended attribute name where tags are stored.
// - maxDepth: The maximum depth to search within the directory tree. A value of -1 means no depth limit.
//
// Returns:
// - A slice of strings containing all unique tags found within the specified depth.
// - An error if the walk fails for reasons other than permission errors.
//
// Example usage:
// tags, err := ListTags("/home/user/docs", "user.tags", 2)
// if err != nil {
//     log.Fatalf("Failed to list tags: %s", err)
// }
// fmt.Println("Unique tags found:", tags)
func ListTags(rootDir, attribute string, maxDepth int) ([]string, error) {
	uniqueTags := make(map[string]struct{})
	baseDepth := len(strings.Split(rootDir, string(os.PathSeparator))) // More accurate depth calculation
	err := filepath.Walk(rootDir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			if os.IsPermission(err) {
				log.Printf("Skipping %s: %v", path, err)
				return filepath.SkipDir // Skip directories where permission is denied
			}
			return err // Stop the walk on other errors
		}

		currentDepth := len(strings.Split(path, string(os.PathSeparator)))
        // Check if the current depth exceeds the maximum allowed depth
		if maxDepth != -1 && currentDepth-baseDepth >= maxDepth {
			if info.IsDir() {
				return filepath.SkipDir // Prevent further traversal into deeper directories
			}
			return nil
		}

		if !info.IsDir() {
			tags, err := fetchTags(path, attribute)
			if err != nil {
				return nil // Ignore files without the attribute or where attributes can't be read
			}
			for _, tag := range tags {
				uniqueTags[tag] = struct{}{} // Store each tag in a set to ensure uniqueness
			}
		}
		return nil
	})

	if err != nil {
		return nil, err
	}

	tagsList := make([]string, 0, len(uniqueTags))
	for tag := range uniqueTags {
		tagsList = append(tagsList, tag)
	}
	return tagsList, nil
}
