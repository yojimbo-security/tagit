/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"fmt"

	"goattr/log"
	"goattr/pkg"

	"github.com/spf13/cobra"
)

// getCmd represents the get command
var getCmd = &cobra.Command{
	Use:   "get",
	Short: "Get tag(s) from file",
	Long: `Get tag(s) from file`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("get called")
        tags, err := pkg.GetTags(filename, attribute)
        if err != nil{
            log.Fatal(err)
        }
        fmt.Println("Tags:")
        for _, tag := range tags.Tags{
            fmt.Printf("  %s\n",tag)
        }
	},
}

func init() {
	rootCmd.AddCommand(getCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// getCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// getCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
